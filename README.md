|                   |                                  |
| ----------------- | -------------------------------- |
| **Project Title** | CFD Project
| Project Start     | 02.11.2020
| Project End       | Ongoing
| Student           | Koray Bora

Purpose & Goals
===========================================================
Design with FreeCAD
Running OpenFoam calculation
Review an OpenFOAM calculation with ParaView
Track files with Git

Actual Project-Status
===========================================================
Review OpenFOAM results with ParaView 

Results
-------------------------------------------------

Open Issues
-------------------------------------------------

Project-Forecast
-------------------------------------------------

Conclusions
===========================================================
