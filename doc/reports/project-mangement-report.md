
<!-- main/short project report with focus to project management -->
<!-- this report refers to the study reports, which contain the detailed simulation information -->


**CFD-Project_**
&emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp; &emsp;
![](tools/images/logo.png)

************************************************  
|                   |                                  |
| ----------------- | -------------------------------- |
| **Project Title** | xxx
| project start     | xxx.2018
| project end       | ongoing
| active stakeholder| xxx
| cfd engineer      | Jan Lehmkuhl
| design engineer   | xxx
| checked by        | xxx
<br>  

<!-- example picture for quick project identification -->
![](doc/images/XXX.png) 


**_Table of Content_**

**********************  
- [Project-Purpose](#project-purpose)
  - [Background](#background)
  - [Goals](#goals)
- [Major Work Packages](#major-work-packages)
  - [Study 1](#study-1)
- [Available Resources](#available-resources)
  - [Calculation Enviroment](#calculation-enviroment)
    - [Hardware](#hardware)
    - [Software](#software)
- [actual Project-Status](#actual-project-status)
  - [Results](#results)
    - [Study 1](#study-1-1)
  - [Open issues](#open-issues)
  - [Project-Forecast](#project-forecast)
- [Conclusions](#conclusions)



Project-Purpose
===============================================================================

Background
---------------------------------------------------------------------
* which information is needed to understand the project goals
* this should be understand by every friend or manager


Goals
---------------------------------------------------------------------
* which goals should be reached within this project
* this is not about simulations. simulations are the tools to reach your goals
* e.g. optimize the flow in something, understand the physics from something
* bulletpoints with max 10 words, more explanations belong to the background



Major Work Packages
===============================================================================
* contains normally one to three simulation studies

Study 1
---------------------------------------------------------------------
* which special information should this study (a specific set simulations) deliver to reach the project goals
* only 4 sentences or bulletpoints. The details belong to the simulation study report



Available Resources
===============================================================================

Calculation Enviroment
---------------------------------------------------------------------
* Short description of the hard- and software used to create the analysis. 
* The objective of this section is to ensure reproducebility of the results for the case of later reruns.

### Hardware
|          |                                                                                        |
| -------- | -------------------------------------------------------------------------------------- |
| Machine  | 
| System   | Host: <br> Kernel: <br> Distro:  <br> Desktop: 
| CPU      | COUNT x Name <br> Cache:  <br> Max. clock speed: XXXX MHz 
| Memory   | Manufacturer: <br> Type: <br>  Speed: XXXX MHz <br> Size: XX x XX GB 
| Graphics | Card: <br> Display Server: <br> Driver: 


### Software
| Task              | Programm                                          |
| ----------------- | ------------------------------------------------- |
| CAD               | 
| stl               | 
| background mesh   | OpenFoam X.x
| mesh              | OpenFoam X.x
| solver            | OpenFoam X.x
| paraview          | paraview X.X.X 
| R version         | X.X.X (20xx-xx-xx)



actual Project-Status
===============================================================================

Results
---------------------------------------------------------------------
### Study 1
* main results of study 1 as short bulletpoint summary from the study report


Open issues
---------------------------------------------------------------------
* where are not yet solved issues or problems which can threaten the project outcome
* e.g. numerics in study 1 are bad, you don't know yet to apply special model features, ...


Project-Forecast
---------------------------------------------------------------------
* next steps and timeframe


Conclusions
===============================================================================
* main outcome of this project in a few bulletpoints
